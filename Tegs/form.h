#ifndef FORM_H
#define FORM_H
#include <QObject>
#include <QWidget>
#include <QtWidgets>
#include <QDebug>
#include "node.h"
#include "linee.h"

class Form:  public QObject
{Q_OBJECT
public:
    Node       mainNode;
    QString    action;
    QString    method;
    QMap <QString, QString> lineNameValue;
    QHBoxLayout vertLayout;

    QWidget *RUN();
    void findNodeChild (Node & node);
    QWidget *thisInput(Node & node);
public slots:
    void click_on_submit();
    void slot_lineE_text_change (QString, QString);
signals:
    void signal_submit (QString, QString, QString);

};

#endif // FORM_H
