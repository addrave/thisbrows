#ifndef LINEE_H
#define LINEE_H
#include <QObject>
#include <QWidget>
#include <QtWidgets>
#include <QDebug>

class LineE : public QLineEdit
{Q_OBJECT
public:
    QString name;


    LineE(QWidget *parent = Q_NULLPTR);
    LineE(const QString &, QWidget *parent = Q_NULLPTR);

    void start();
public slots:
    void line_text_change(QString text);
signals:
    void signal_lineE_text_change(QString, QString);
};

#endif // LINEE_H
