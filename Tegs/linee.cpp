#include "linee.h"

LineE::LineE(QWidget *parent)
    : QLineEdit (parent)
{
}


LineE::LineE(const QString &, QWidget *parent)
    : QLineEdit (parent)
{
}


void LineE::start()
{
    QObject::connect(this, SIGNAL(textChanged(QString)),
                     this, SLOT(line_text_change(QString))
                     );
}

void LineE::line_text_change(QString text)
{
    emit signal_lineE_text_change(text, name);
}
