#ifndef A_H
#define A_H

#include <QtWidgets>
#include <QObject>
#include "parser.h"

class Teg_A_Label : public QLabel
{
Q_OBJECT
public:
    Teg_A_Label (QWidget *parent=Q_NULLPTR, Qt::WindowFlags f=Qt::WindowFlags());
    Teg_A_Label (const QString &text, QWidget *parent=Q_NULLPTR, Qt::WindowFlags f=Qt::WindowFlags());

    void create_label(QString text, QString link);
    void mousePressEvent(QMouseEvent *event);

    QString link;
signals:
    void click_on_link_in_teg_a (QString);

};


#endif // A_H
