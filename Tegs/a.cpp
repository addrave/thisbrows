#include "Tegs/a.h"


Teg_A_Label::Teg_A_Label(QWidget *parent, Qt::WindowFlags f)
    : QLabel (parent, f)
{
}

Teg_A_Label::Teg_A_Label(const QString &text, QWidget *parent, Qt::WindowFlags f)
    : QLabel (text, parent, f)
{
}

void Teg_A_Label::create_label(QString text, QString link)
{
    this->setText(text);
    this->link = link;
    QPalette * tempPal = new QPalette (this->palette());
    tempPal->setColor(QPalette::Foreground, Qt::blue);          //QPalette::WindowText
    this->setPalette(*tempPal);
    //this->setStyleSheet("color: rgb(0, 0, 255)");       //Альтернативный способ менять цвет
    this->setFixedSize(this->sizeHint());
    this->setCursor(Qt::PointingHandCursor);
}


//Teg_A_Label::~Teg_A_Label()
//{
//;
//}

void Teg_A_Label::mousePressEvent(QMouseEvent *event)
{
//    qDebug () << this->link;
    emit click_on_link_in_teg_a (link);

}
