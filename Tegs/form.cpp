#include "form.h"


QWidget * Form::RUN()
{
    findNodeChild(mainNode);
    QWidget *tempW = new QWidget;
    tempW->setLayout(&vertLayout);
    tempW->setFixedSize(tempW->sizeHint());
    return tempW;
}


void Form::findNodeChild(Node & node)
{
    if (node.tag_name == "input")
    {
        QWidget *tempW = new QWidget;
        tempW = thisInput(node);
        if(tempW != NULL)
            vertLayout.addWidget(tempW);
    }
    for (int i(0); i < node.child.size(); i++)
    {
        findNodeChild(*node.child.at(i));
    }
}


QWidget * Form::thisInput(Node & node)
{
    //    qDebug() << node.attributes;

    QString type = "text";
    for (int i(0); i < node.attributes.keys().size(); i++)
    {
        if (node.attributes.keys().at(i).contains("type"))
        {
            type = node.attributes.value(node.attributes.keys().at(i));
            type.toLower();
            type.replace (' ',"");
            type.replace ('"', "");
        }
    }
    if (type == "submit")
    {
        QString nameButton = "Отправить";
        for (int i(0); i < node.attributes.keys().size(); i++)
        {
            if (node.attributes.keys().at(i).contains("value"))
            {
                nameButton = node.attributes.value(node.attributes.keys().at(i));
                nameButton.toLower();
                nameButton.replace (' ',"");
                nameButton.replace ('"', "");
            }
        }
        QPushButton *submit = new QPushButton (nameButton);
        submit->setFixedSize(submit->sizeHint());
        QObject::connect(submit, SIGNAL(clicked()),
                         this, SLOT(click_on_submit())
                         );
        return submit;
    }
    if (type == "text")
    {
        QString nameLE;
        for (int i(0); i < node.attributes.keys().size(); i++)
        {
            if (node.attributes.keys().at(i).contains("name"))
            {
                nameLE = node.attributes.value(node.attributes.keys().at(i));
                nameLE.toLower();
                nameLE.replace (' ',"");
                nameLE.replace ('"', "");
            }
        }
        LineE * lineE = new LineE;
        lineE->name = nameLE;
        lineE->start();
        lineE->setFixedSize(200,25);

        QObject::connect(lineE, SIGNAL(signal_lineE_text_change(QString,QString)),
                         this, SLOT(slot_lineE_text_change(QString,QString))
                         );
        return lineE;
    }
    return NULL;
}


void Form::click_on_submit()
{
    //    qDebug() << lineNameValue;
    QString link = action;
    QString values;
    for (int i(0); i < lineNameValue.keys().size(); i++)
    {
        if(!lineNameValue.value(lineNameValue.keys().at(i)).isEmpty())
        {
            if(i != 0) values.push_back("&");
            values.push_back(lineNameValue.keys().at(i));
            values.push_back("=");
            values.push_back(lineNameValue.value(lineNameValue.keys().at(i)));
        }
    }
    //qDebug() << values << link;
    if (method == "get")
    {
        if (action.contains("?"))
        {
            if (!values.isEmpty())
            {
                action.push_back("&");
                action.push_back(values);
            }
        }
        else
        {
            if (!values.isEmpty())
            {
                action.push_back("?");
                action.push_back(values);
            }
        }
//        qDebug() << action;
        emit signal_submit (method, action, values);
    }
    if (method == "post")
    {
        emit signal_submit (method, action, values);
    }

}



void Form::slot_lineE_text_change(QString text, QString name)
{
    lineNameValue.insert(name, text);
}

