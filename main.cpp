#include <stdlib.h>
#include <iostream>
#include <QDebug>
#include <QFile>
#include <sstream>
#include <QWidget>
#include <QObject>
#include <QScrollArea>
#include <QApplication>
#include <QLabel>
#include <windows.h>
#include <QTextCodec>
#include <QTextDecoder>

#include <iostream>
#include <conio.h>

#include <QRegExp>

#include <QNetworkAccessManager>

#include "node.h"
#include "parser.h"

#include "other_Form/mainwindow.h"

using namespace std;


int main(int argc, char* argv[])
{
    setlocale(LC_ALL, "Russian");

    QApplication app(argc, argv);
    MainWindow w;
    w.show();

    QObject::connect(&w.h, SIGNAL(signal_hGoB_clicked (QString)),
                     &w, SLOT(slot_hGoB_clicked (QString))
                    );

    return app.exec();
}
