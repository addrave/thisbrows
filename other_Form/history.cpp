#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QIODevice>
#include "history.h"
#include "ui_history.h"
#include "mainwindow.h"

History::History(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::History)
{
    ui->setupUi(this);
    flag1 = true;
    indexRow = 0;
    sizeHistoryList = 0;
    //
    historyFile.setFileName("HistoryForm");
    if (historyFile.exists())
    {
        qDebug () << "exist";
        QString tempFile;
        if(historyFile.open(QFile::ReadOnly | QFile::Text))
        {
            qDebug () << "is open";
            tempFile = historyFile.readAll();
        }
        QString tempString;
        QString::iterator it = tempFile.begin();
        for (; it != tempFile.end(); it++)
        {
            if (*it == '\n'/* || it == tempFile.end()-1*/)
            {
                add_line_to_history(tempString);
                tempString.clear();
            }
            else
                tempString.push_back(*it);
        }

        historyFile.close();
    }
    else
    {
        qDebug () << "yet not exist. file will be created";
        if(historyFile.open(QFile::WriteOnly | QFile::Text))
            qDebug () << "is open";
        historyFile.close();
    }
}

History::~History()
{
    delete ui;
}


void History::on_pushButton_2_clicked() // delete url
{
    if (/*sizeHistoryList*/ui->listWidget->count() == 1)
    {
        on_DeleteAll_clicked();
    }
    else
    {
        if (flag1)
        {
            historyFile.open(QFile::ReadOnly | QFile::Text);
            QString tempString = ui->listWidget->item(indexRow)->text();
            QString tempFile = historyFile.readAll();
            historyFile.close();

            tempFile.remove(tempString + '\n');
            historyFile.open(QFile::WriteOnly | QFile::Text);
            QTextStream hStream (&historyFile);
            hStream << tempFile;
            historyFile.close();


            ui->listWidget->takeItem(indexRow); // удаление элемента
            flag1 = false;
            sizeHistoryList--;
        }
    }
}

void History::on_DeleteAll_clicked() // delete all url
{
    ui->listWidget->clear();
    indexRow = 0;
    indexRow2 = 0;
    sizeHistoryList = 0;
    historyFile.remove();

}

void History::on_listWidget_currentRowChanged(int currentRow)
{
    indexRow = currentRow;
    //qDebug() << "iR" << indexRow;
    flag1 = true;
}

void History::add_line_to_history (const QString & item)
{
    bool temp_bool = true;
    for (int i = 0; i<ui->listWidget->count()/*sizeHistoryList*/;i++) //длинна списка
    {
        if (ui->listWidget->item(i)->text() == item)
        {
            temp_bool = false;
        }
    }
    if (temp_bool)
    {
        ui->listWidget->insertItem(0,item);
        sizeHistoryList++;

        historyFile.open(QIODevice::ReadOnly | QIODevice::Text);
        QString tempString = historyFile.readAll();
        tempString.insert(tempString.size(),item); tempString.insert(tempString.size(),'\n');
        historyFile.close();
        historyFile.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream hStream (&historyFile);
        hStream << tempString;
        historyFile.close();
    }
}

QString History::get_line_on_history ()
{
    if (ui->listWidget->count() > 0)
    {
        QString  url;
        url = ui->listWidget->item(indexRow)->text();  //    qDebug() << url;
        return url;
    }
    else
    {
        QString  url = "список строк пуст"; // внимательно с этим!!!
        return url;
    }
}

void History::on_hGoB_clicked()
{ if (ui->listWidget->count() > 0)
    {
        QString  url;
        url = ui->listWidget->item(indexRow)->text();
        emit signal_hGoB_clicked(url);
    }
    else
    {
        QString  url = "список строк пуст";
        //return url;
    }
}
