#ifndef COOKIE_H
#define COOKIE_H

#include <QMap>
#include <QMainWindow>
#include <QFile>

namespace Ui {
class Cookie;
}

class Cookie : public QMainWindow
{
    Q_OBJECT

public:
    explicit Cookie(QWidget *parent = 0);
    ~Cookie();

    void add_or_change_cookie_on_list (QString domain, QByteArray cookie);

    QByteArray get_cookie_by_domain (QString & domain);

    void updating();

private slots:
    void on_listWidget_currentRowChanged(int currentRow);

    void on_deleteCooB_clicked();

    void on_deleteAllCooB_clicked();

    void on_deleteForDomainCooB_clicked();

    void on_deleteForDomenCooB_clicked();

signals:
    void signal_all_cookies_clear();

public:   //privat
    Ui::Cookie *ui;

    int currentRow;
};

#endif // COOKIE_H
