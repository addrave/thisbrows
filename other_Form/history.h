#ifndef HISTORY_H
#define HISTORY_H

#include <QFile>
#include <QTextStream>
#include <QIODevice>
#include <QObject>
#include <QMainWindow>

namespace Ui {
class History;
}

class History : public QMainWindow//, public QObject
{
    Q_OBJECT

public:
    explicit History(QWidget *parent = 0);
    ~History();

    void add_line_to_history (const QString & item);
    QString get_line_on_history ();
    QFile historyFile;

public:          //privat
    Ui::History *ui;
    int indexRow;
    int indexRow2;
    int sizeHistoryList;
    bool flag1;

public slots:
    void on_hGoB_clicked();

private slots:

    void on_pushButton_2_clicked();
    void on_DeleteAll_clicked();
    void on_listWidget_currentRowChanged(int currentRow);

signals:

    void signal_hGoB_clicked(QString);
};
#endif // HISTORY_H
