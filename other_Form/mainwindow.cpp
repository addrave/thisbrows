//setWindowTitle("MyBrowser");
#include "parser.h"

#include "history.h"
#include "ui_history.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "Tegs/a.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    postReq = false;
    ui->setupUi(this);
    outOfSizeList = 0; //    ui->tabWidget->count();
    outOfSizeList2 = 0;
    indexTabBar = 0; // важно
    ui->progressBar->hide();


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    url_ = arg1;
}

void MainWindow::on_goB_clicked()
{
    if (vecTabs->size() == 0)
    {
        on_addB_clicked();
    }
    urlList.push_back(url_);
    add_item_on_list();
    vecTabs->at(indexTabBar)->indexUrlTab = vecTabs->at(indexTabBar)->urlListTab.size() - 1;  // сбивает указатель на url при навигации LEFT RIGHT!
    vecTabs->at(indexTabBar)->indexUrlTab2 = vecTabs->at(indexTabBar)->urlListTab.size() - 1; // эта дичь нужна для LEFT RIGHT
}
void MainWindow::add_item_on_list()
{
    QWidget * tempW = new QWidget;
    tempW = get_http_text (url_);
    //
    //QLabel *temp = new QLabel; temp->setText(tree);
    //tempW->show();
    ui->tabWidget->removeTab(indexTabBar);
    tempW->resize(tempW->sizeHint());
    ui->tabWidget->insertTab(indexTabBar,tempW,tabName);
    ui->tabWidget->setCurrentIndex(indexTabBar);




    //
    vecTabs->at(indexTabBar)->mainLabelTab = tempW;
    //vecTabs->at(indexTabBar)->mainLabelTab->resize(vecTabs->at(indexTabBar)->mainLabelTab->sizeHint()); // ВАЖНАЯ ШТУКА ПОДГОНА РАЗМЕРА ДЛЯ SkrollArea
    vecTabs->at(indexTabBar)->urlListTab.push_back(url_);
    h.add_line_to_history(url_);
    //
    //vecTabs->at(indexTabBar)->mainLabelTab->resize(vecTabs->at(indexTabBar)->mainLabelTab->sizeHint()); // ВАЖНАЯ ШТУКА ПОДГОНА РАЗМЕРА ДЛЯ SkrollArea

}

QWidget * MainWindow::get_http_text (QString url_)
{
    ui->progressBar->show(); ui->lineEdit->hide(); ui->progressBar->setValue(15);
    parser  = new Parser;
    network = new Network;

    if (postReq == false)
    {
    network->get(url_);
    parser->change_codec(network->mainHTML);
    }
    else
    {
        network->post_req(url_, postValues);
        parser->change_codec(network->mainHTML);
        postValues.clear();
        postReq = false;
    }

//        parser->get_html(); // считать сраницу из файла по нажатию ОК

    QLabel *temp = new QLabel;
    //temp->setText(html);
    QScrollArea * localTempScA = new QScrollArea;
    localTempScA->setWidget(temp);
    localTempScA->resize(500,500);
    ui->progressBar->setValue(70);
    Render_this_shit * tempRender = new Render_this_shit;                                            // очень важная штука
    QObject::connect(tempRender, SIGNAL(signal_change_window_title(QString)),
                     this, SLOT(slot_change_window_title(QString))
                     );
    QObject::connect(tempRender, SIGNAL(click_on_link_in_teg_a(QString)),
                     this, SLOT(slot_click_on_link_in_teg_a (QString))
                     );
    QObject::connect(tempRender, SIGNAL(signal_post_request(QString,QString)),
                     this, SLOT(slot_post_request(QString,QString))
                     );
    QWidget * tempW = new QWidget;
    tempRender->titleString = url_;
    tempRender->urlString = url_;
    tempW = tempRender->GO(*parser->root);

    ui->progressBar->hide(); ui->lineEdit->show();

    delete network;
    delete parser;
    return tempW;
}


void MainWindow::on_historyB_clicked()
{
    h.show();

}

void MainWindow::on_leftB_clicked()
{

    if (!vecTabs->empty())           //если вектор не пуст
    {
        if (vecTabs->at(indexTabBar)->indexUrlTab > 0)
        {
            vecTabs->at(indexTabBar)->indexUrlTab --;
            vecTabs->at(indexTabBar)->urlListTab.push_back(vecTabs->at(indexTabBar)->urlListTab[vecTabs->at(indexTabBar)->indexUrlTab]);
            ui->lineEdit->setText(vecTabs->at(indexTabBar)->urlListTab[vecTabs->at(indexTabBar)->indexUrlTab]);
            //vecTabs->at(indexTabBar)->mainLabelTab->setText(get_http_text(vecTabs->at(indexTabBar)->urlListTab[vecTabs->at(indexTabBar)->indexUrlTab]));
            vecTabs->at(indexTabBar)->mainLabelTab->resize(vecTabs->at(indexTabBar)->mainLabelTab->sizeHint()); // ВАЖНАЯ ШТУКА ПОДГОНА РАЗМЕРА ДЛЯ SkrollArea


            QWidget * tempW = new QWidget;
            tempW = get_http_text (url_);

            ui->tabWidget->removeTab(indexTabBar);
            tempW->resize(tempW->sizeHint());
            ui->tabWidget->insertTab(indexTabBar,tempW,tabName);
            ui->tabWidget->setCurrentIndex(indexTabBar);
        }
    }
}

void MainWindow::on_rightB_clicked()
{
    if (!vecTabs->empty())
    {
        if (vecTabs->at(indexTabBar)->indexUrlTab < vecTabs->at(indexTabBar)->indexUrlTab2)
        {
            vecTabs->at(indexTabBar)->indexUrlTab ++;
            vecTabs->at(indexTabBar)->urlListTab.push_back(vecTabs->at(indexTabBar)->urlListTab[vecTabs->at(indexTabBar)->indexUrlTab]);
            ui->lineEdit->setText(vecTabs->at(indexTabBar)->urlListTab[vecTabs->at(indexTabBar)->indexUrlTab]);

            vecTabs->at(indexTabBar)->mainLabelTab->resize(vecTabs->at(indexTabBar)->mainLabelTab->sizeHint()); // ВАЖНАЯ ШТУКА ПОДГОНА РАЗМЕРА ДЛЯ SkrollArea


            QWidget * tempW = new QWidget;
            tempW = get_http_text (url_);

            ui->tabWidget->removeTab(indexTabBar);
            tempW->resize(tempW->sizeHint());
            ui->tabWidget->insertTab(indexTabBar,tempW,tabName);
            ui->tabWidget->setCurrentIndex(indexTabBar);
        }
    }
}

void MainWindow::on_addB_clicked()
{
    Tabs *tab = new Tabs;
    QScrollArea *sa = new QScrollArea;
    tab->mainLabelTab = new QLabel;
    tab->indexTab = vecTabs->size();

    char b [16];
    sprintf(b, "%d", tab->indexTab);
    tab->headerTab = b;

    ui->tabWidget->insertTab(vecTabs->size(), sa, tab->headerTab);

    sa->setWidget(tab->mainLabelTab);

    vecTabs->push_back(tab);

}


void MainWindow::on_tabWidget_tabBarClicked(int index)
{
    indexTabBar = index;
    if (vecTabs->at(indexTabBar)->urlListTab.size() > 0)
        ui->lineEdit->setText(vecTabs->at(indexTabBar)->urlListTab[vecTabs->at(indexTabBar)->indexUrlTab]);
    if (vecTabs->at(indexTabBar)->urlListTab.empty())
        ui->lineEdit->setText("");
}

void MainWindow::on_deleteB_clicked()
{
    if (vecTabs->size() != 0)
    {
        if (indexTabBar == vecTabs->size()-1)
        {
            vecTabs->remove(indexTabBar);
            ui->tabWidget->removeTab(indexTabBar);
            if (indexTabBar != 0)
                indexTabBar--;
        }
        else {
            vecTabs->remove(indexTabBar);
            ui->tabWidget->removeTab(indexTabBar);
            for (int i = indexTabBar; i<vecTabs->size(); i++)
            {
                vecTabs->at(i)->indexTab --;
            }
        }
    }
}
void MainWindow::on_historyB_2_clicked()
{
    c.updating();
    c.show();
}

void MainWindow::update_tab ()    //странный способ обновить вкладку
{
    ui->tabWidget->insertTab(indexTabBar,vecTabs->at(indexTabBar)->mainLabelTab,vecTabs->at(indexTabBar)->headerTab);
    ui->tabWidget->setCurrentIndex(indexTabBar);
}

void MainWindow::on_reB_clicked()
{if (ui->tabWidget->count() != 0)
    {
        QWidget * tempW = new QWidget;
        tempW = get_http_text (url_);

        ui->tabWidget->removeTab(indexTabBar);
        tempW->resize(tempW->sizeHint());
        ui->tabWidget->insertTab(indexTabBar,tempW,tabName);
        ui->tabWidget->setCurrentIndex(indexTabBar);
    }
}

void MainWindow::change_headerTab (QString & item)
{
    vecTabs->at(indexTabBar)->headerTab = item;
    //update_tab();
}

void MainWindow::change_mainLaibelTab (QLabel * item) // важно не передавать в разные вкладки указатель на один и тот же QLabel. иначе все идет по пизде
{
    vecTabs->at(indexTabBar)->mainLabelTab->deleteLater(); // ???
    vecTabs->at(indexTabBar)->mainLabelTab = item;


    ui->tabWidget->insertTab(indexTabBar,vecTabs->at(indexTabBar)->mainLabelTab,vecTabs->at(indexTabBar)->headerTab);
    ui->tabWidget->setCurrentIndex(indexTabBar);
}

void MainWindow::slot_hGoB_clicked(QString url)
{
    url_ = url;
    on_goB_clicked();
    ui->lineEdit->setText(url_);

}

void MainWindow::slot_change_progress_bar(int volue)
{
    ui->progressBar->setValue(20+volue);
}

void MainWindow::slot_all_cookies_clear()
{
}

void MainWindow::slot_change_window_title(QString item)
{
    setWindowTitle(item);

    tabName = item;

}

void MainWindow::slot_click_on_link_in_teg_a (QString item)
{
    url_ = item;
    on_goB_clicked();
    ui->lineEdit->setText(url_);
}

void MainWindow::slot_post_request(QString link, QString values)
{
    postReq = true;
    url_ = link;
    postValues = values;
    on_goB_clicked();
    ui->lineEdit->setText(url_);
}
