#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include "history.h"
#include "ui_history.h"
#include "cookie.h"
#include "ui_cookie.h"
#include "tabs.h"
#include <QMainWindow>
#include <QtWidgets>
#include <QObject>



#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QString>
#include <QList>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QTextCodec>

#include <Tegs/a.h>
#include "Tegs/a.h"
#include "Tegs/render_this_shit.h"

#include "parser.h"
#include "network.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void add_item_on_list();

    void update_tab ();

    void change_headerTab (QString & item);

    void change_lineTextTab (QString & item);

    void change_mainLaibelTab (QLabel * item);

    QWidget *get_http_text(QString url_);

    Node & pars_this_shit(Node & node, int depth);


public slots:

    void slot_change_window_title(QString item);

    void slot_click_on_link_in_teg_a (QString item);

    void slot_post_request (QString link, QString values);

private slots:
    void on_lineEdit_textChanged(const QString &arg1);

    void on_goB_clicked();

    void on_historyB_clicked();

    void on_leftB_clicked();

    void on_rightB_clicked();

    void on_addB_clicked();

    void on_tabWidget_tabBarClicked(int index);

    void on_deleteB_clicked();

    void on_historyB_2_clicked();

    void on_reB_clicked();

    void slot_hGoB_clicked(QString url);

    void slot_change_progress_bar (int volue);

    void slot_all_cookies_clear ();


public:                      //private
    Ui::MainWindow *ui;
    QList <QString> urlList;         // общая история. она нужна
    QVector <Tabs*> *vecTabs = new QVector <Tabs*>;
    QString url_;
    int outOfSizeList, outOfSizeList2; //    ui->tabWidget->count();

    int langthV, indexTabBar;

    QMap <QString, QByteArray> cookie;      // куууууки
    History h;
    Cookie c;
    Parser         * parser;
    Network        * network;
    QString tabName;

    //
    QLabel tempLabel;
    QVBoxLayout tempVLayout;
    QScrollArea tempScroll;

    bool postReq;
    QString postValues;
};

#endif // MAINWINDOW_H
