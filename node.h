#ifndef NODE_H
#define NODE_H

#include <QList>
#include <QMap>

struct Node
{
    typedef QMap<QString,QString> Attribute;

    Node* parent;

    QList<Node*> child;

    QString tag_name;
    Attribute attributes;
};

#endif // NODE_H
