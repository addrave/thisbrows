#ifndef NETWORK_H
#define NETWORK_H
#include <stdlib.h>
#include <iostream>
#include <QDebug>
#include <QFile>
#include <sstream>
#include <QWidget>
#include <QObject>
#include <QScrollArea>
#include <QApplication>
#include <QLabel>
#include <windows.h>
#include <QTextCodec>
#include <QTextDecoder>

#include <iostream>
#include <conio.h>

#include <QRegExp>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

#include <QNetworkCookie>
#include <QNetworkCookieJar>


#include "node.h"

class Network :public QObject
{Q_OBJECT
public:
    QMap <QString, QString>     cookieMap;
    QNetworkAccessManager     * manager;
    QNetworkReply             * reply;

    QByteArray mainHTML;
    QByteArray postRequestString;

    QUrl       urlUser;

    void       get (QString item);
    void       insert_cookie ();
    QString    get_cookie_for_url (QString item);
    QWidget *download_IMG(QString item);
public slots:
    void       replyFinished();

//------------------------------------------------//

    void post_req(QString item, QString param);
public slots:
    void getReplyFinished ();
    void readyReadReply ();


signals:
    void eLoopQuit();
};

#endif // NETWORK_H
