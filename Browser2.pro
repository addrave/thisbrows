QT += core
QT += gui
QT += network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


CONFIG += c++11

TARGET = Browser
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    parser.cpp \
    other_Form/cookie.cpp \
    other_Form/history.cpp \
    other_Form/mainwindow.cpp \
    Tegs/a.cpp \
    Tegs/render_this_shit.cpp \
    network.cpp \
    Tegs/form.cpp \
    Tegs/linee.cpp

HEADERS += \
    node.h \
    parser.h \
    other_Form/cookie.h \
    other_Form/history.h \
    other_Form/mainwindow.h \
    other_Form/tabs.h \
    Tegs/a.h \
    Tegs/render_this_shit.h \
    network.h \
    Tegs/form.h \
    Tegs/linee.h

FORMS += \
    other_Form/cookie.ui \
    other_Form/history.ui \
    other_Form/mainwindow.ui
