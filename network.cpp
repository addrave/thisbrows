#include "network.h"

void Network::get(QString item)
{
    urlUser.setUrl(item);
    manager = new QNetworkAccessManager();
    QNetworkRequest request(urlUser);
    //
    QString thisCookie;

    thisCookie = get_cookie_for_url(item);
    if (!thisCookie.isEmpty())
        request.setRawHeader("Cookie", thisCookie.toUtf8());
//    qDebug() << "send coookie" << thisCookie.toUtf8();
    //request.setHeader(QNetworkRequest::CookieHeader, "Cookie");
    /*QNetworkReply * */reply = manager->get(request);

    QObject::connect ( reply, SIGNAL(finished()),
                       this, SLOT(replyFinished())
                       );
    QEventLoop event_loop;
    QObject::connect ( reply, SIGNAL(finished()),
                       &event_loop, SLOT(quit())
                       );
    event_loop.exec();
    //    QEventLoop event_loop;                                                      //
    //    QObject::connect(this, SIGNAL(done()), &event_loop, SLOT(quit()));          // Wait for download this page! ура, я могу в ангийском
    //    event_loop.exec();                                                          //


}

void Network::replyFinished()
{
    //    emit done();                                               // ВАЖНО. ждем срабатывания слота replyFinished, потому что именно тогда завершается загрузка файла
    //QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if (reply->error() == QNetworkReply::NoError)
    {
        if (!manager->cookieJar()->cookiesForUrl(urlUser).empty())
        {
            insert_cookie ();
        }
        mainHTML = reply->readAll();

        //////////////////        ЗАПИСАТЬ СТРАНИЦУ В ФАЙЛ
        //                        QFile in1;
        //                        in1.setFileName("C:/Users/stoun_000/Desktop/temp6/WRITE.txt");
        //                        in1.open(QIODevice::WriteOnly);
        //                        if (in1.isOpen()) qDebug () << "in1 is open"; else qDebug () << "in1 NOT open";
        //                        in1.write(mainHTML);
    }
    else
    {
        qDebug()<< "MyNetwork .. reply->errorString()" << reply->errorString();

    }

    reply->deleteLater();
}


QWidget * Network::download_IMG(QString item)
{
    manager = new QNetworkAccessManager(this);
    QUrl url(item);
    QNetworkReply* reply = manager->get(QNetworkRequest(url));

    QEventLoop eventloop1;
    connect(reply,SIGNAL(finished()),&eventloop1,SLOT(quit()));
    eventloop1.exec();

    if (reply->error() == QNetworkReply::NoError)
    {
        QByteArray data = reply->readAll();
        QImage image = QImage::fromData(data);
        if (!image.isNull())
        {
            QLabel * label1 = new QLabel;
            label1->setPixmap(QPixmap::fromImage(image));
            label1->setFixedSize(label1->sizeHint());
            //        label1->show();

            return label1;
        }
    }
    else
    {
        reply->errorString();
    }
    return NULL;
}

void Network::insert_cookie()
{
    QFile cookieOut;
    cookieOut.setFileName("cookie");
    if (!cookieOut.exists())
    {
        ;
    }
    else
    {
        if (!cookieOut.open(QIODevice::ReadOnly))
            qDebug() << "file cookie cannot open file for riding";
        QDataStream in(&cookieOut);
        in.setVersion(QDataStream::Qt_4_1);
        quint32 n;
        in >> n >> cookieMap;
//        qDebug() << "in >> cookieMap" << cookieMap;
        cookieOut.remove();
        cookieOut.close();
    }


    //
    QString strCookieDomain = manager->cookieJar()->cookiesForUrl(urlUser).at(0).domain();
    QString strCookieName   = manager->cookieJar()->cookiesForUrl(urlUser).at(0).name();
    QString strCookieValue  = manager->cookieJar()->cookiesForUrl(urlUser).at(0).value();
    QString strCookieNAndV; strCookieNAndV.push_back(strCookieName); strCookieNAndV.push_back('='); strCookieNAndV.push_back(strCookieValue);
    //qDebug() << strCookieDomain << strCookieNAndV;
    cookieMap.insert(strCookieDomain, strCookieNAndV);
    //
    cookieOut.open(QIODevice::WriteOnly);
    cookieOut.isOpen() ? qDebug () << "cookieOut is create" : qDebug () << "cookieOut NOT create";

    QDataStream out(&cookieOut);
    out.setVersion(QDataStream::Qt_4_1);
    out << quint32(0x12345678) << cookieMap;
    cookieOut.close();
}

QString Network::get_cookie_for_url(QString item)
{
    QString thisCookie;
    QFile cookieOut;
    cookieOut.setFileName("cookie");
    if (!cookieOut.exists())
    {
        qDebug() << "cFile cookie not found";
    }
    else
    {
        if (!cookieOut.open(QIODevice::ReadOnly))
            qDebug() << "file cookie cannot open file for reading";
        QDataStream in(&cookieOut);
        in.setVersion(QDataStream::Qt_4_1);
        quint32 n;
        in >> n >> cookieMap;
//        qDebug() << "in >> cookieMap (get)" << cookieMap;
        cookieOut.close();
    }
    //qDebug () << "prov" << cookieMap->empty();
    if (!cookieMap.empty())
        for (int c(0); c < cookieMap.keys().size(); c++)
        {
            if (item.contains(cookieMap.keys().at(c)))
            {
                thisCookie = cookieMap.value(cookieMap.keys().at(c));
            }
        }
    return thisCookie;
}

void Network::post_req(QString item, QString param)
{
    postRequestString = param.toUtf8();
    urlUser.setUrl(item);
    manager = new QNetworkAccessManager();
    //    apiUrl = "http://shotinleg.ru/idb-15-16/index.php";
    //    requestString = "str=q";
    QNetworkRequest request(urlUser);

    QString thisCookie;

    thisCookie = get_cookie_for_url(item);
    if (!thisCookie.isEmpty())
        request.setRawHeader("Cookie", thisCookie.toUtf8());
    qDebug() << "send coookie" << thisCookie.toUtf8();

    request.setRawHeader("Content-Type","application/x-www-form-urlencoded");
    request.setRawHeader("User-Agent","Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");

    request.setHeader(QNetworkRequest::ContentLengthHeader, postRequestString.length());
    reply = manager->post(request, postRequestString);

    QEventLoop eventLoop;
    QObject::connect(reply, SIGNAL(finished()),this, SLOT(getReplyFinished()));
    QObject::connect(reply, SIGNAL(readyRead()), this, SLOT(readyReadReply()));
    QObject::connect(this, SIGNAL(eLoopQuit()), &eventLoop, SLOT(quit()));
    eventLoop.exec();
}


void Network::getReplyFinished ()
{
    reply->deleteLater();

}


void Network::readyReadReply ()
{
    mainHTML = reply->readAll();
    if (!manager->cookieJar()->cookiesForUrl(urlUser).empty())
    {
        insert_cookie ();
    }
    emit eLoopQuit();
}

