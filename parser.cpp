#include "parser.h"

void Parser::get_html() // СЧИТАТЬ СТРАНИЦУ ИЗ ФАЙЛА
{
    QFile in1;
    in1.setFileName("C:/Users/stoun_000/Desktop/temp6/WRITE.html");
    in1.open(QIODevice::ReadOnly);
    if (in1.isOpen()) qDebug () << "in1 is open"; else qDebug () << "in1 NOT open";
    QByteArray qba1 = in1.readAll();
    QString string1 = QString::fromUtf8(qba1);
    QTextCodec* defaultTextCodec = QTextCodec::codecForName("Windows-1251");
    QTextDecoder *decoder = new QTextDecoder(defaultTextCodec);
    QString string2 = decoder->toUnicode(qba1);
    if (string1.contains("а") && string2.contains("а"))
        qDebug() << "\nWAAAAT 0.1";
    if (string1.contains("а") && !string2.contains("а"))
        html = string1;
    if (!string1.contains("а") && string2.contains("а"))
        html = string2;
    if (!string1.contains("а") && !string2.contains("а"))
        html = string1;
    //qDebug() << html;
    this->pars_html();
}

void Parser::change_codec (QByteArray & item) // GO
{
    QString string1 = QString::fromUtf8(item);
    QTextCodec* defaultTextCodec = QTextCodec::codecForName("Windows-1251");
    QTextDecoder *decoder = new QTextDecoder(defaultTextCodec);
    QString string2 = decoder->toUnicode(item);
    if (string1.contains("а") && string2.contains("а"))
    {
        qDebug() << "\nWAAAAT 0.1";
        html = string1;
    }
    if (string1.contains("а") && !string2.contains("а"))
        html = string1;
    if (!string1.contains("а") && string2.contains("а"))
        html = string2;
    if (!string1.contains("а") && !string2.contains("а"))
        html = string1;

    //qDebug() << this->html;
    this->pars_html();
    //this->print_tree(root,0);
}

void Parser::pars_html()
{
    root->parent = NULL;
    root->tag_name = "ITSAPRANKITSAPRANK";
    html.replace("\n"," "); // не уверен что правильно
    html.replace("\t","");
    html.replace("<br>", "");
    html.replace("/>", ">");

    QString tempString;
    //    QString counterString;                    // для отладки
    Node * valid_node = new Node;
    valid_node = root;
//    qDebug() << valid_node->tag_name;
    Tag_type tag_type = DOCTYPE;
    QString::iterator it = html.begin();
    for( ; it!=html.end(); it++)
    {
        //        if (counterString.size() < 200)
        //            counterString += *it;
        //        else
        //        {
        //            counterString.remove(0,1);
        //            counterString += *it;
        //        }
        if (tag_type == START_TAG)
        {
            tempString += *it;
            if (*it == '>')
            {
                tempString.remove(tempString.size()-1,1);
                QMap <QString,QString> attribute;
                QString tag_name;
                QString attributeName;
                QString attributeValue;
                Node_values node_value = TAG_NAME;
                QString::iterator tempIt = tempString.begin();
                for ( ; tempIt != tempString.end(); tempIt++)
                {
                    if (node_value == NAME_ATTR)
                        attributeName += *tempIt;
                    if (*tempIt == ' ' && node_value == TAG_NAME)
                        node_value = NAME_ATTR;
                    if (node_value == TAG_NAME)
                        tag_name += *tempIt;
                    if (node_value == VALUE_ATTR)
                        attributeValue += *tempIt;
                    if (*(tempIt-1) == '=' && *tempIt == '"' && node_value == NAME_ATTR)
                        node_value = VALUE_ATTR;
                    if (*(tempIt-1) == '"' && *tempIt == ' ' && node_value == VALUE_ATTR)
                    {
                        attributeName  = attributeName.toLower();
                        //attributeValue = attributeValue.toLower(); //12.03
                        attribute.insert(attributeName,attributeValue);
                        attributeName.clear();
                        attributeValue.clear();
                        node_value = NAME_ATTR;
                    }
                    if (*tempIt == '"' && tempIt == tempString.end() - 1 && node_value == VALUE_ATTR)
                    {
                        attributeName  = attributeName.toLower();
                        //attributeValue = attributeValue.toLower(); //12.03
                        attribute.insert(attributeName,attributeValue);
                    }
                }

                Node * node = new Node;
                tag_name = tag_name.toLower();
                node->tag_name = tag_name;
                node->attributes = attribute;
                node->parent = valid_node;
                valid_node->child.push_back(node);
                valid_node = node;

                tag_type = TEXT;
                tempString.clear();
                //qDebug() << tag_name << attribute;
            }
        }
        if (tag_type == END_TAG)
        {
            tempString += *it;
            if (*it == '>')
            {
                tempString.remove(0,1);
                tempString.remove(tempString.size()-1,1);
                tempString = tempString.toLower();
                bool isNotGood = false;
                Node * tempNode = valid_node;
                while (isNotGood == false && valid_node->tag_name != tempString)
                {
                    valid_node = valid_node->parent;
                    if (valid_node->tag_name == "ITSAPRANKITSAPRANK")
                        isNotGood = true;
                }
                if (isNotGood == false)
                    if (valid_node->tag_name == tempString)
                    {
                        valid_node = valid_node->parent;
                        //
                        tag_type = TEXT;       // warning
                        tempString.clear();
                    }
                if (isNotGood == true)
                {
                    valid_node = tempNode;
//                    qDebug() << "isNotGood" << "missing teg " << tempString;
                }
            }

        }
        if (tag_type == TEXT)
        {
            tempString += *it;
            if (*it == '<')
            {
                tempString.remove(0,1);
                tempString.remove(tempString.size()-1,1);
                tempString.replace('\n', "");
                tempString.replace('\t', "");
                tempString.replace("&lt;","<");
                tempString.replace("&#060;", "<");
                tempString.replace("&gt;",">");
                tempString.replace("&#062;", ">");
                tempString.replace("&amp;", "&");
                tempString.replace("&#038;", "&");
                tempString.replace("&nbsp;", "");
                tempString.replace("&#160;", "");

                if(!tempString.isEmpty())
                {
                    bool flagNotSpace = false;
                    QString::iterator iter = tempString.begin();
                    for (; iter!= tempString.end(); iter++)
                        if (*iter != ' ')
                            flagNotSpace = true;
                    if (flagNotSpace == true)
                    {
                        Node * node = new Node;
                        node->tag_name = "text";
                        node->attributes.insert("text", tempString);
                        node->parent = valid_node;
                        valid_node->child.push_back(node);
                        //qDebug() << tempString;
                    }
                }
                tempString.clear();
            }
        }
        if (tag_type == DOCTYPE && *it == '>')
        {
            tag_type = NONE;
        }
        if (*it == '<' && tag_type != INDEPENDENT_TAG)
        {
            if (*(it+1) == '/')
            {
                tag_type = END_TAG;
                tempString.clear();
            } else if (*(it+1) == '!' && *(it+2) == '-' && *(it+3) == '-')
            {
                tag_type = INDEPENDENT_TAG;
                tempString.clear();
            }
            else
            {
                tag_type = START_TAG;
                tempString.clear();
            }
            if (it == html.begin() && *(it+2) == 'D')
                tag_type = DOCTYPE;
        }
        if (*it == '>' && *(it-1) == '-' && *(it-2) == '-' && tag_type == INDEPENDENT_TAG)
            tag_type = START_TAG;

    }

//    emit done();
}

void Parser::print_tree(Node *node, int depth)
{
    qDebug() << depth << node->tag_name;
    for (int i(0); i < node->child.size(); i++)
        print_tree(node->child.at(i), depth+1);

}

