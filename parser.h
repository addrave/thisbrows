#ifndef PARSER_H
#define PARSER_H

#include <stdlib.h>
#include <iostream>
#include <QDebug>
#include <QFile>
#include <sstream>
#include <QWidget>
#include <QObject>
#include <QScrollArea>
#include <QApplication>
#include <QLabel>
#include <windows.h>
#include <QTextCodec>
#include <QTextDecoder>

#include <iostream>
#include <conio.h>

#include <QRegExp>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>


#include "node.h"
#include <QNetworkCookie>

using namespace std;

enum Tag_type {TEXT             = 0,
               START_TAG        = 1,
               END_TAG          = 2,
               INDEPENDENT_TAG  = 3,
               DOCTYPE          = 4,
               NONE             = 5};
enum Node_values  {TAG_NAME         = 0,
                   NAME_ATTR        = 1,
                   VALUE_ATTR       = 2};

class Parser : public QObject
{Q_OBJECT
public:
    QString html;


    Node * root = new Node;
    void get_html ();
    void pars_html ();
    void print_tree (Node * node, int depth);
    void change_codec (QByteArray & item);
};
#endif // PARSER_H
